jQuery(document).ready(function($) {

    /*-----------------------------------------------------------------------------------*/
    /*  Fixed Scrolling
    /*-----------------------------------------------------------------------------------*/ 
    var page = $(document),
        body = $('body'),
        hasScrolled = 'hasScrolled',
        parallax = $('.parallax');

        if (parallax.length){
            $pxScrolled = '239';
        } else {
            $pxScrolled = '0';
        }      

        page.scroll(function() {
          body.toggleClass(hasScrolled, page.scrollTop() >= $pxScrolled);
        });

    /*-----------------------------------------------------------------------------------*/
    /*  Background Chooser
    /*-----------------------------------------------------------------------------------*/
    var bgPicker = $(".bgPicker");
    var bgChooser = $(".bgChooser");
    var inner = $(".closeBgChooser, .bgChooser ul li");
    var closeBgChooser =  $(".closeBgChooser");
        
    $(".bgPicker").click(function (e) {
        e.preventDefault(); 
        if (bgPicker.hasClass('active')) {
            inner.fadeOut(100, function () {
                bgChooser.slideUp(200); 
                bgPicker.removeClass('active');         
            });
            return false;
        }   
        bgPicker.addClass('active');    
        bgChooser.slideDown(200, function () { 
            inner.fadeIn(300);
        });
    });
    
    closeBgChooser.click(function (e) {     
        inner.fadeOut(100, function () {
            bgChooser.slideUp(200); 
            bgPicker.removeClass('active');         
        });
        return false;
    }); 

    var body = $("body");
    $(".bgChooser ul li").click(function (e) {
        e.preventDefault();
        $.cookie('cookieBg',null,{ expires: -1, path: '/'});
        var id = $(this).attr('id');
        $.cookie('cookieBg',id,{ expires: 365, path: '/'});
        body.removeClass().addClass(id);
    });
    
    if (($.cookie('cookieBg') != null)) {
        body.addClass($.cookie('cookieBg'));
    } else {
        body.addClass("bg-default");
    }

});
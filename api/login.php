<?php

require('../src/XF.php');
XF::start('/');
$XF = XF::setupApp('XF\Pub\App');
//$XF->start();

$input       = $_REQUEST['username'] ? $_REQUEST['username'] : null;
$password    = $_REQUEST['password'] ? $_REQUEST['password'] : null;

$ip = $XF->request()->getIp();
$service = $XF->service('XF:User\Login', $input, $ip);
$user = $service->validate($password, $error);

//completeLogin
$XF->session()->changeUser($user);

XF::setVisitor($user);
$XF->repository('XF:SessionActivity')->clearUserActivity(0, $ip);
$XF->repository('XF:Ip')->logIp(
    $user->user_id, $ip,
    'user', $user->user_id, 'login'
);

//createVisitorRememberKey
$rememberRepo = $XF->repository('XF:UserRemember');
$key = $rememberRepo->createRememberRecord($user->user_id);

$value = $rememberRepo->getCookieValue($user->user_id, $key);

//$XF->response()->setCookie('user', $value, 365 * 86400); // Uh, didn't work...
setcookie('xf_user', $value, time() + (365 * 86400), '/', 'forum.scandinaver.local'); // ugly

echo json_encode(['name' => 'xf_user', 'value' => $value]);
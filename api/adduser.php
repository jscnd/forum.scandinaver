<?php

$dir = __DIR__;
require('../src/XF.php');

XF::start($dir);
$app = XF::app();

$username = $_REQUEST['username'] ? $_REQUEST['username'] : null;
$email    = $_REQUEST['email'] ? $_REQUEST['email'] : null;
$password = $_REQUEST['password'] ? $_REQUEST['password'] : null;

/**
 * @var \XF\Entity\User $user
 */
$user = $app->repository('XF:User')->setupBaseUser();

$user->username = $username;
$user->email = $email;
$user->Auth->setPassword($password);
$result  = $user->save();

echo ($result) ? 'success' : 'fail';

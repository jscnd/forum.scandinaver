<?php

$dir = realpath(__DIR__."../../");
require('../src/XF.php');
//var_dump($dir);
XF::start($dir);
$app = XF::app();

$username = (isset($_REQUEST['username'])) ? $_REQUEST['username'] : null;
$email    = (isset($_REQUEST['email']))    ? $_REQUEST['email']    : null;
$newemail = (isset($_REQUEST['newemail'])) ? $_REQUEST['newemail'] : null;
$avatar   = (isset($_REQUEST['avatar']))   ? $_REQUEST['avatar']   : null;
$password = (isset($_REQUEST['password'])) ? $_REQUEST['password'] : null;

/**
 * @var \XF\Entity\User $user
 */
$user = $app->repository('XF:User')->getUserByNameOrEmail($email);
/** @var \XF\Entity\UserAuth $userAuth */
$userAuth = $user->getRelationOrDefault('Auth');

if($username)
    $user->set('username', $username);

if($newemail)
    $user->set('email', $newemail);

if($password)
    $userAuth->setPassword($password);

$user->save();

if($avatar){
    $tempFile = \XF\Util\File::getTempFile();

    if ($app->http()->reader()->getUntrusted($avatar, [], $tempFile, ['verify' => false]))
    {
        /** @var \XF\Service\User\Avatar $avatarService */
        $avatarService = $app->service('XF:User\Avatar', $user);
        if (!$avatarService->setImage($tempFile))
        {
            return false;
        }

         $avatarService->updateAvatar();
    }
    else
    {
        return false;
    }
}

$result  = $user->save();

echo ($result) ? 'success' : 'fail';


